package com.example.eventos

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val buttononclick: Button = findViewById(R.id.ButtononClick)
        val buttononLongclick: Button = findViewById(R.id.buttononLongClick)
        val editextonFocus: EditText = findViewById(R.id.onFocusChange)


        buttononclick.setOnClickListener { onClick() }
        buttononLongclick.setOnLongClickListener {
            onLongClick()
            return@setOnLongClickListener true
        }

        var v: View
        var s: Boolean
        editextonFocus.setOnFocusChangeListener { v, s ->
            onFocusChanger()
        }

    }

    fun onClick() {
        Toast.makeText(this, "Hola soy un evento onclick en android ", Toast.LENGTH_SHORT).show()
    }

    fun onLongClick() {
        Toast.makeText(this, "Hola soy un evento onLongClick en android ", Toast.LENGTH_SHORT)
            .show()
    }

    fun onFocusChanger() {
        Toast.makeText(this, "Ingresa un correo", Toast.LENGTH_SHORT).show()
    }

}