# En las interfaces de los receptores de eventos, se incluyen los siguientes métodos de devolución de llamada:

### onClick()
Desde View.OnClickListener. Se llama a este método cuando el usuario toca el elemento (en el modo táctil), o selecciona el elemento con las teclas de navegación o la bola de seguimiento y presiona la tecla Intro o la bola de seguimiento adecuada.
### onLongClick()
Desde View.OnLongClickListener. Se llama a este método cuando el usuario toca y mantiene presionado el elemento (en el modo táctil), o selecciona el elemento con las teclas de navegación o la bola de seguimiento y mantiene presionada la tecla Intro o la bola de seguimiento adecuada (durante un segundo).
### onFocusChange()
Desde View.OnFocusChangeListener. Se llama a este método cuando el usuario navega hacia el elemento o sale de él utilizando las teclas de navegación o la bola de seguimiento.
### onKey()
Desde View.OnKeyListener. Se llama a este método cuando el usuario se centra en el elemento y presiona o suelta una tecla física del dispositivo.
### onTouch()
Desde View.OnTouchListener. Se llama a este método cuando el usuario realiza una acción calificada como un evento táctil, por ejemplo, presionar, soltar o cualquier gesto de movimiento en la pantalla (dentro de los límites del elemento).
### onCreateContextMenu()
Desde View.OnCreateContextMenuListener. Se llama a este método cuando se crea un menú contextual (como resultado de un "clic largo" sostenido). Consulta la explicación sobre menús contextuales en la guía para desarrolladores Menús.
Estos métodos son los únicos habitantes de su respectiva interfaz. Para definir uno de estos métodos y manejar sus eventos, implementa la interfaz anidada en su actividad o defínela como una clase anónima. Luego, pasa una instancia de tu implementación al método View.set...Listener() respectivo. (Por ejemplo, llama a setOnClickListener() y pásale tu implementación de OnClickListener).